## Readme
### App url: https://app-worker-order.herokuapp.com/
### content-type application/json

### Create a Worker
```
POST /worker
@params {
    "name": "name",
    "company_name": "company name",
    "email": "youremail@example.com"
}
```


### Delete a worker
```
DELETE /worker/:worker_id
@params worker_id _id field of worker
```


### Create Work Order
```
POST /order
@params {
    "title": "title",
    "description: "description",
    "deadline": "yyyy/mm/dd"
}
```

### Assign a worker to an order
```
POST /order/:order_id/assign
@params order_id order _id field
@params {
    "worker_id": "worker _id field"
}
```

### Fetch all orders for a specific worker and sort by deadline
```
GET /worker/:worker_id/order/:sort?
@params worker_id _id field of worker
@params sort (optional) sort parameter either 'asc' or 'desc' default to 'desc'
```