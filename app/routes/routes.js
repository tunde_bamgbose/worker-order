// define routes and export
module.exports = (app)=>{

    // bring in conttollers
    var workerController = require('../controllers/WorkerController');
    var orderController = require('../controllers/OrderController');


    // create new worker
    app.post('/worker', workerController.create);

    // delete worker
    app.delete('/worker/:worker_id', workerController.delete);

    // create an order
    app.post('/order', orderController.create);

    // assign an order to a worker
    app.post('/order/:order_id/assign', orderController.assign);

    // list worker's assigned orders
    app.get('/worker/:worker_id/order/:sort?', orderController.list);

    // handle routes not defined
    app.get('*',(req,res)=>{
        res.status(404).send({status:false,error:"Not Found"});
    })
};