// bring in express
var express = require('express');

// get config
var config = require('./config/config');

// initialize express
var app = express();

// bring in body parser
var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))


// get routes
require('./routes/routes')(app);

// connect to mongodb using mongoose
var mongoose = require('mongoose');
mongoose.connect(`mongodb://${config.db.user}:${config.db.password}@ds337377.mlab.com:37377/hatchways`,{useNewUrlParser: true});

var db = mongoose.connection;

// log error encountered with mongodb
db.on("error",(error)=>{
    console.log("DB Error: ",error);
});

// create server
var server = require("http").createServer(app);

// start server
server.listen(process.env.PORT || config.app.port,()=>{
    var host = server.address().address;
    var port = server.address().port;
    console.log(`Server started. Listening at ${host}:${port}`);
})
