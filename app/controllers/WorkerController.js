// bring worker model
var Worker = require('../models/worker');

/**
 * Function registers a worker
 */
exports.create = async (req,res)=>{
    try{
        // create a new instance of worker
        var worker = new Worker(req.body);

        // check if user with email already exists
        let existingUser = await Worker.findOne({"email": req.body.email});

        // if user exist don't allow registration, else register new worker
        if(existingUser){
            res.status(409).send({status: false, error: "Worker with email address already registered"});
        }
        else{
            // try and save new user
            let newUser = await worker.save();

            // check if user was actually saved. if not return error
            if(newUser)
                res.status(200).send({status:true, message: "Worker successfully registered!", data: newUser});
            else    
                res.status(500).send({status: false, error: "Internal Server Error"}); 
        }

    }
    catch(error){
        res.status(400).send({status: false, error: error});
    }
}

/**
 * Delete a worker using worker_id
 */
exports.delete = async (req,res)=>{
    try{
        // get worker_id parameter from request url
        var id = req.params.worker_id;

        // delete worker with id
        let success = await Worker.deleteOne({"_id": id});

        // check if user was actually deleted. if not return error
        if(success)
            res.status(200).send({status:true, message: "Worker deleted!"});
        else
            res.status(500).send({status: false, error: "Internal Server Error"}); 
    }
    catch(error){
        res.status(400).send({status: false, error: error});
    }
}