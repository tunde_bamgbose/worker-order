// require order model
var Order = require('../models/order');
var WorkOrder = require('../models/workOrder');
var Worker = require('../models/worker');

// get config
var config = require('../config/config').app;

var mongoose = require('mongoose');

/**
 * Create an order
 */
exports.create = async (req,res)=>{
    try{
        // create new instance of order
        var order = new Order(req.body);

        // save order
        var newOrder = await order.save();

        // check if order was saved, if not return error
        if(newOrder)
            res.status(200).send({status: true, message: "Work order created!", data: newOrder});
        else
            res.status(500).send({status: false, error: "Internal Server Error"}); 
    }
    catch(error){
        res.status(400).send({status: false, error: error});
    }
};

/**
 * Assign an order to a worker
 */
exports.assign = async(req,res) => {
    try{
        // get order_id parameter from request url
        var order_id = req.params.order_id;
        var worker_id = req.body.worker_id;

        // check if worker is a registered worker, if not reject request
        var user = await Worker.findById(worker_id);

        if(!user){
            res.status(404).send({status: false, error: "User not found"});
            return;
        }

        // check if order is belongs to an order, if not reject request
        var order = await Order.findById(order_id);

        if(!order){
            res.status(404).send({status: false, error: "Order not found"});
            return;
        }

        // create new WorkOrder object
        var workOrder = new WorkOrder({ worker_id: worker_id, order_id: order_id  });

        // check if worker has not been added to order
        let record = await WorkOrder.findOne({ worker_id: worker_id, order_id: order_id  });

        // if user has a record, reject request else goto next check
        if(record){
            res.status(409).send({status:false, message: "Worker has already been assigned to this order"});
        }
        else{

            // check if the max number of workers hav been assgined to order with order_id
            let count = await WorkOrder.find({ order_id: order_id}).countDocuments();

            if( count < config.max_workers_allowed){

                let newWorkOrder = await workOrder.save();

                // check if request was fulfilled, if not return error
                if(newWorkOrder){
                    res.status(200).send({status: true, message: "Worker assigned to order", data: newWorkOrder});
                }
                else
                    res.status(500).send({status: false, error: "Internal Server Error"}); 
            }
            else{
                // max workers assigned
                res.status(400).send({status: false, error: "Maximum number of workers assigned"}); 
            }
        }
    }
    catch(error){
        res.status(400).send({status: false, error: error});
    }
}

/**
 * fetch all work orders single user
 * if sort parameter is passed, sort by deadline
 */
exports.list = async(req,res)=>{
    try{
        // get worker_id parameter from url
        var worker_id = req.params.worker_id;

        // get sort query param
        var query = req.params.sort;

        var list;

        switch(query){
            case 'asc':
                // sort by deadline ascending
                list = await WorkOrder.aggregate([
                    {$match: { "worker_id": mongoose.Types.ObjectId(worker_id)  } },
                    {
                        $lookup: {
                            from: "orders",
                            localField: 'order_id', 
                            foreignField: '_id', 
                            as: 'order'
                        }
                    },
                    { $sort : { 'order.deadline' : 1}}
                ]);
                break;
            default:
                // sort by deadline descending
                list = await WorkOrder.aggregate([
                    {$match: { "worker_id": mongoose.Types.ObjectId(worker_id)  } },
                    {
                        $lookup: {
                            from: "orders",
                            localField: 'order_id', 
                            foreignField: '_id', 
                            as: 'order'
                        }
                    },
                    { $sort : { 'order.deadline' : -1}}
                ]);
                break;
        }

        if(list)
            res.status(200).send({ status: true, data: list});
        else
            res.status(500).send({status: false, error: "Internal Server Error"}); 

    }
    catch(error){
        res.status(400).send({status: false, error: error});
    }
};