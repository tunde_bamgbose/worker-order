var mongoose = require('mongoose');

// define schema for work order
var orderSchema = mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    },
    deadline:{
        type: Date,
        required: true
    },
    total_workers:{
        type: Number,
        default: 0
    }
});

// define model and export
var Order = module.exports = mongoose.model('Order',orderSchema);