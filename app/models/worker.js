var mongoose = require('mongoose');

// define schema for worker
var workerSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    company_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
});

// define model and export
let Worker = module.exports = mongoose.model('Worker',workerSchema);