var mongoose = require('mongoose');

// define schema
var workOrderSchema = mongoose.Schema({
    worker_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Worker'
    },
    order_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Order'
    },
});

// define model and export
var WorkOrder = module.exports = mongoose.model('WorkOrder',workOrderSchema);